export interface UserInterface {
    id              : number,
    username        : string,
    email           : string,
    email_verify    : string,
    password        : string,
    password_verify : string,
    full_name?  : string,
    dni             : string,
    phone           : string,
    state           : string,
    city            : string,
    neighborhood    : string,
    address         : string,
    salary          : number,
    others_income   : number,
    month_expenses  : number,
    financial_expences: number,
    created_at      : string,
    updated_at      : string
}   
