import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = {
    email: 'mail2@mail.com',
    password: '123456'
  };


  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit()
  {
    this.authService.login(this.user)
      .subscribe( 
        res => {
          console.log(res)
          if (res.success) {
            localStorage.setItem('token',res.dat.token);
            this.router.navigate(['/user'])
          } else {
            alert(res.message);
          }
        },
        err => {
          console.log(err)
          alert(err)
        }
      )
  }
}
