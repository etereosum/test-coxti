import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../services/auth.service';
import { UserInterface } from '../../interfaces/user.interface';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  user:any;

  constructor(private userService: UserService,
              private authService: AuthService) { 

  }

  ngOnInit(): void {
    this.getUser()
  }

  getUser () {
    this.userService.getUser()
      .subscribe(
        res => {

          console.log(res)
          if (res.success) {
            this.user = res.dat;
          }
        },
        err => console.log(err)
      )
  }

  delete(){

    if (!confirm('Do you want to delete this account')) {
      return false;
    }

    this.userService.delete(this.user)
      .subscribe(
        res => {
          if (res.success) {
            this.authService.logOut();
          }
        }
      )
  }

}
