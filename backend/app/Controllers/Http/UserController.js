'use strict'
const User = use('App/Models/User');

class UserController {

    async show ({ auth, response }) {
        
        let user = await auth.getUser();
        return response.json({
            success: true,
            dat: user
        });
    }

    async update ({ request, auth, params, response }) {

        const data = request.except(['email_confirm', 'password_confirm'])

        await User
            .query()
            .where( 'id', params.id )
            .update( data );

        let user = await User.find(params.id);

        return response.json({
            success: true,
            dat: user
        });
    }

    async delete ({ auth, params, response }) {

        try 
        {
            let user = await User.find( params.id );
            await user.delete();
            
            return response.json({ 
                success: true,
                message: 'User has been deleted' 
            });

        } catch (e) {
            console.log( e );
            return response.json({
                success: false,
                message: 'An error has occurred'
            })
        }
    }
    


}

module.exports = UserController
