import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private URL = 'http://localhost:3333/api'; 

  constructor(private http: HttpClient,
    private router: Router) { }

  getUser () {
    return this.http.get<any>(this.URL + '/user')
  }

  register (user:any) {
    return this.http.post<any>(this.URL + '/register',user);
  }

  update (user:any) {
    return this.http.put<any>(this.URL + '/user/'+user.id,user);
  }

  delete (user:any) {
    return this.http.delete<any>(this.URL + '/user/'+user.id);
  }
  
}
