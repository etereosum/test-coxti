'use strict'
const User = use('App/Models/User');
const { validate } = use('Validator');

class AuthController {

    /**
     * Register user
     * @returns user with token
     * 12042020 etereosum@gmail.com
     */

    async register ({ request, auth, response }) {

        const data = request.except(['email_confirm', 'password_confirm'])

        const rules = {
            username: 'required|unique:users,username',
            full_name: 'required',
            phone: 'required',
            dni: 'required|unique:users,dni',
            email: 'email|required|unique:users,email',
            password: 'required|min:6'
        };

        const validation = await validate(data, rules);

        if (validation.fails()) {
      
            return response.json({
                success: false,
                errors: validation.messages()
            });
          }

        let user = await User.create( data );

        // generate token from user
        let token = await auth.generate( user );

        Object.assign( user, token );

        return response.send({
            success: true,
            dat: user
        });
        
    }//.register

    /**
     * Login user
     * @returns user with token
     * 12042020 etereosum@gmail.com
     */

    async login ({ request, auth, response }) {

        let { email, password } = request.all();

        try {

            if ( await auth.attempt( email, password )) {
                let user  = await User.findBy( 'email', email );
                let token = await auth.generate( user );

                Object.assign( user, token );
                return response.json({
                    success: true,
                    message: 'You are logged',
                    dat: user
                });

            } else {
                return response.json({ 
                    success: false,
                    message: 'Invalid credentials'
                });
            }

        } catch ( e ) {
            console.log( e );
            return response.json({ 
                success: false,
                message: 'Invalid credentials'
            })
        }

    }//.login
    
}

module.exports = AuthController
