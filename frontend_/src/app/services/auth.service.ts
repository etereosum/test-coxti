import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private URL = 'http://localhost:3333/api'; 

  constructor(private http: HttpClient,
    private router: Router) { }

  register (user) {
    return this.http.post<any>(this.URL + '/register', user);
  }

  login (credentials) {
    return this.http.post<any>(this.URL + '/login', credentials);
  }

  getToken () {
    return localStorage.getItem('token');
  }

  loggedIn () {
    return !!localStorage.getItem('token');
  }

  logOut () {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}
