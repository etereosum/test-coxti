import { Component, OnInit } from '@angular/core';
import { UserInterface } from '../../interfaces/user.interface';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user = {
    username        : 'username',
    full_name       : 'pablo gonzalez',
    phone           : '3333333',
    dni             : '987654231',
    email           : 'mail2@mail.com',
    email_confirm   : 'mail2@mail.com',
    password        : '123456',
    password_confirm: '123456',
    state           : 'Risaralda',
    city            : 'Pereira',
    neighborhood    : 'Centro',
    address         : 'Cr 4 N 3-24',
    salary          : '1000000',
    others_income   : '200000',
    month_expenses  : '500000',
    financial_expences : '50000'
  }

  validPass:boolean = false;
  validEmail:boolean = false;
  status: string;
  errors: string[];

  constructor(private userService:UserService,
              private router: Router,
              private activateRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.validateEmail();
    this.validatePass();

    this.activateRoute.params.subscribe(paramas => {
      this.status =  paramas.status;
    });

    if ( this.status == 'update' ) {
      this.getUser();
      this.validEmail = true;
      this.validPass = true;
    }

    console.log('status: ',this.status)
  
  }

  onSubmit() {

    this.errors = null;
    if (!this.validations()){
      return false;
    }

    if (this.status == 'create') {
      this.create();
    } else {
      this.update();
    }
  }

  create() {
    this.userService.register(this.user)
      .subscribe(
        res => {
            console.log(res);
            if (res.success) {

              localStorage.setItem('token',res.dat.token);
              this.router.navigate(['/user'])
            } else {
              this.errors = res.errors;
              console.log(res.errors)
            }
        },
        err => console.log(err)
      )
  }

  update() {
    this.userService.update(this.user)
      .subscribe(
        res => {
          console.log(res);
          if (res.success) {
            this.router.navigate(['/user'])
          }
          else {
            console.log(res)
          }
        },
        err => console.log(err)
      )
  }

  validations(){
    this.validateEmail();
    this.validatePass();
  
    if(!this.validEmail || !this.validPass) {
      alert('It is required to validate the email and password');
      return false;
    }

    return true;
  }

  validatePass(){
    console.log(this.user.password_confirm);
    if (this.user.password == this.user.password_confirm) {
      this.validPass = true;
    } else {
      this.validPass = false;
    }
  }

  validateEmail(){
    console.log(this.user.email_confirm)
    if (this.user.email == this.user.email_confirm) {
      this.validEmail = true;
    } else {
      this.validEmail = false;
    }
  }


  getUser() {
    this.userService.getUser()
      .subscribe(
        res => {

          console.log(res)
          if (res.success) {
            this.user = res.dat;
          }
        },
        err => console.log(err)
      )
  }
}
