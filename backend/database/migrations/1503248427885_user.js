'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('username', 80).notNullable().unique()
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()
      table.string('full_name',200)
      table.string('dni',30)
      table.string('phone',12)
      table.string('state',20)
      table.string('city',20)
      table.string('neighborhood',20)
      table.string('address',200)
      table.decimal('salary',10,2)
      table.decimal('others_income',10,2)
      table.decimal('month_expenses',10,2)
      table.decimal('financial_expences',10,2)
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
